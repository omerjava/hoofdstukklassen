import enumOpdracht.Day;
import enumOpdracht.Planet;

public class PlanetApp {
    public static void main(String[] args) {

        for (Planet element : Planet.values()) {
            System.out.println(element.name()+ " " + element.ordinal());
            System.out.println(element);
        }
    }
}
