package opdrachtShapeAbstractClass;

public class Square extends Rectangle {
    private static int count;
    {
        count++;
    }

    private int side;

    public Square() {
        super();
    }

    public Square(int width, int height) {
        super(width, height);
    }

    public Square(int x, int y, int side) {
        super(x, y);
        setSide(side);
    }

    public Square(Square square) {
        this(square.getX(), square.getY(), square.getSide());
    }

    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        super.setWidth(side);
        super.setHeight(side);
    }

    @Override
    public void setWidth(int width) {
        setSide(width);
    }

    @Override
    public void setHeight(int height) {
        setSide(height);
    }

    public static int getCount(){
        return count;
    }

    @Override
    public String toString() {
        return "Square{" +
                "side=" + side +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Square square = (Square) o;

        return side == square.side;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + side;
        return result;
    }
}
