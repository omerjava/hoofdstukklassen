package opdrachtShapeAbstractClass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Drawing {

    public Drawing() {}
    private int size;
    Shape[] shapeObjects = new Shape[0];


    public void add(Shape shape){
        List<Shape> shapeList = new ArrayList<>(Arrays.asList(shapeObjects));
        if(shapeList.size()<=100) {
            shapeList.add(shape);
        } else {
            System.out.println("There is no more place in Array!");
        }
        shapeObjects = shapeList.toArray(shapeObjects);
        if(shapeObjects[shapeObjects.length-1].equals(shapeList)) {
            System.out.println("Test is ok. Given shape is added to Tekening..");
        }
        else {
            System.out.println("Oh..no! Something wrong!");
        }
    }

    public void remove(Shape shape){
        List<Shape> drawableList = new ArrayList<>(Arrays.asList(shapeObjects));
        drawableList.remove(shape);
        shapeObjects = drawableList.toArray(shapeObjects);
        if(!shapeObjects[shapeObjects.length-1].equals(shape)) {
            System.out.println("Test is ok. Given shape is removed from Tekening..");
        }
        else {
            System.out.println("Oh..no! Something wrong!");
        }
    }

    public void clear(){
        List<Shape> shapeList = new ArrayList<>(Arrays.asList(shapeObjects));
        shapeList.clear();
        shapeObjects = shapeList.toArray(shapeObjects);
    }

    public int getSize(){
        size = shapeObjects.length;
        return size;
    }


    @Override
    public String toString() {
        return "Drawing{" +
                "size=" + size +
                ", shapeObjects=" + Arrays.toString(shapeObjects) +
                '}';
    }
}
