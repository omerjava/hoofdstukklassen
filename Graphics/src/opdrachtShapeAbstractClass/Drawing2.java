package opdrachtShapeAbstractClass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Drawing2 {

    public Drawing2() {}

    private int size;
    Shape[] shapeObjects = new Shape[0];


    public void add(Shape shape){
        List<Shape> shapeList = new ArrayList<>(Arrays.asList(shapeObjects));
        if(shapeList.size()<=100) {
            shapeList.add(shape);
        } else {
            System.out.println("There is no more place in Array!");
        }
        shapeObjects = shapeList.toArray(shapeObjects);
        if(shapeObjects[shapeObjects.length-1].equals(shape)) {
            System.out.println("Test is ok. Given shape is added to Tekening..");
        }
        else {
            System.out.println("Oh..no! Something wrong!");
        }
    }

    public boolean isPresent(Shape shape){
        return Arrays.asList(shapeObjects).contains(shape);
    }

    public boolean isPresent2(Shape shape){
        for (Shape element : shapeObjects) {
            if(element.equals(shape)) return true;
        }
        return false;
    }

    public int getFreeLocation(Shape shape){
        int index=0;

        for(int i=0; i<shapeObjects.length; i++){
            //if (shapeObjects[i])
        }

        return index;
    }

    public void remove(Shape shape){
        List<Shape> shapeList = new ArrayList<>(Arrays.asList(shapeObjects));
        shapeList.remove(shape);
        shapeObjects = shapeList.toArray(shapeObjects);
        if(!shapeObjects[shapeObjects.length-1].equals(shape)) {
            System.out.println("Test is ok. Given shape is removed from Tekening..");
        }
        else {
            System.out.println("Oh..no! Something wrong!");
        }
    }

    public void clear(){
        List<Shape> shapeList = new ArrayList<>(Arrays.asList(shapeObjects));
        shapeList.clear();
        shapeObjects = shapeList.toArray(shapeObjects);
    }

    public int getSize(){
        size = shapeObjects.length;
        return size;
    }


}
