import opdrachtShapeAbstractClass.*;

public class OpdrachtPolymorphismApp {
    /*
    Opdracht: Polymorfisme
Maak een array van tien willekeurige figuren
Druk voor elke figuur de omtrek, de oppervlakte, de positie op het scherm af
Druk ook de afmetingen van de figuren af. Test hiervoor het object met de operator instanceof en
voer vervolgens een downcasting uit. Gebruik vervolgens de juiste methoden om de afmetingen op te vragen
Optioneel: maak een array van tien willekeurige dieren en laat al deze dieren geluid maken
     */
    public static void main(String[] args) {
        Shape[] shapes = new Shape[10];
        shapes[0] = new Circle(4);
        shapes[1] = new Rectangle(3,4,5,6);
        shapes[2] = new Square(5,6,4);
        shapes[3] = new Circle(3);
        shapes[4] = new Rectangle(2,4,3,5);
        shapes[5] = new Circle(9);
        shapes[6] = new Square(3,-5, 9);
        shapes[7] = new Rectangle(6,6,5,3);
        shapes[8] = new Square(5,8,8);
        shapes[9] = new Triangle(4,5,3,3,2);

        int count=0;
        for (Shape shape: shapes) {
            System.out.println(shape.getClass().getName()+" Shape " + count+1 + " info:");
            printShape(shape);
            count++;
        }

    }

    public static void printShape(Shape shape){
        System.out.println("Shape Area: " + shape.getArea());
        System.out.println("Shape Perimeter: " + shape.getPerimeter());
        System.out.println("Shape X, Y coordinates: " + shape.getX() + ", " +shape.getY());
        // afmetingen
        if(shape instanceof Circle){
            System.out.println("Shape Radius: " + ((Circle) shape).getRadius());
        }
        else if (shape instanceof Triangle){
            System.out.println("Shape Width, Height, Perpendicular: " + ((Triangle) shape).getWidth() + ", "
                    + ((Triangle) shape).getHeight() + ", " + ((Triangle) shape).getPerpendicular());
        }
        else if (shape instanceof Rectangle){
            System.out.println("Shape Width, Height: " + ((Rectangle) shape).getWidth() + ", "
                    + ((Rectangle) shape).getHeight());
        }
        System.out.println("");
    }
}
