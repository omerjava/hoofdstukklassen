import myShapes.Rectangle;
import myShapes.Square;

public class SquareApp {

    public static void main(String[] args) {
        Square square = new Square();
        square.setPosition(8,8);
        square.setHeight(6);
        square.setWidth(6);

        System.out.println("Square Area: "+square.getArea());
        System.out.println("Square Perimeter: "+square.getPerimeter());
        System.out.println("How many square is created: "+Square.getCount());

        Square square1 = new Square(5, 9, 6, 6);

        Square square2 = new Square();
        square2.setSide(9);
        square2.setWidth(8);

        System.out.println("Square2 Area: "+square2.getArea());

        Rectangle squareRect = new Square();
    }
}
