import interfaceOpdracht.*;

public class ShapeOpdrachtApp {

    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(4 ,6, 8,9);

        System.out.println("rect1 area: " + rect1.getArea());
        System.out.println("rect1 perimeter: " + rect1.getPerimeter());
        rect1.grow(12);
        System.out.println("rect1 area: " + rect1.getArea());
        System.out.println("rect1 perimeter: " + rect1.getPerimeter());

        Square square1 = new Square(7, 9, 8);
        Rectangle square2 = new Square(8, 9, 10);
        Shape square3 = new Square(4,3,4);

        System.out.println("square1 area: " + square1.getArea());
        System.out.println("square1 perimeter: " + square1.getPerimeter());
        System.out.println("square2 area: " + square2.getArea());
        System.out.println("square2 perimeter: " + square2.getPerimeter());
        System.out.println("square3 area: " + square3.getArea());
        System.out.println("square3 perimeter: " + square3.getPerimeter());
        System.out.println("square count: " + Square.getCount());
        System.out.println("rectangle count: " + Rectangle.getCount());


        Triangle triangle1 = new Triangle(4, 5, 6);
        Shape triangle2 = new Triangle(5, 9,8);
        System.out.println("triangle1 area: " + triangle1.getArea());
        System.out.println("triangle1 perimeter: " + triangle1.getPerimeter());
        System.out.println("triangle count: " + Triangle.getCount());


        Circle circle1 = new Circle(3);
        Circle circle2 = new Circle(5);
        System.out.println("circle1 area: " + circle1.getArea());
        System.out.println("circle1 perimeter: " + circle1.getPerimeter());
        System.out.println("circle count: " + Circle.getCount());

        Shape circle3 = new Circle(6);
        System.out.println("circle3 area: " + circle3.getArea());
        System.out.println("circle3 perimeter: " + circle3.getPerimeter());
        System.out.println("circle count: " + Circle.getCount());

        System.out.println("circle1 String: " + circle1.toString());
        System.out.println("square1 String: " + square1.toString());
        System.out.println("rect1 String: " + rect1.toString());
        System.out.println("triangle1 String: " + triangle1.toString());

        Square square4 = new Square(3,4,9);
        Rectangle square5 = new Square(3,4,9);

        System.out.println("check Equality: " + square4.equals(square5));


    }
}
