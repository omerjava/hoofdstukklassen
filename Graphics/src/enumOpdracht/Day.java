package enumOpdracht;

/*
In deze opdracht maken we een opsommingstype voor de dagen van de week

Maak een opsomming Day met de dagen van de week
Voeg een private variabele (boolean) toe waarin wordt aangegeven of dit een weekdag is
Vervang de methode toString() zodat ook weergegeven wordt of dit een weekdag is
Maak een hoofdprogramma dat alle dagen van de week overloopt en de naam en ordinale waarde afdrukt
 */

public enum Day {
    MONDAY(true),
    TUESDAY(true),
    WEDNESDAY(true),
    THURSDAY(true),
    FRIDAY(true),
    SATURDAY(false),
    SUNDAY(false);

    private boolean isWeekday;

    Day(boolean isWeekday) {
        this.isWeekday = isWeekday;
    }

    public boolean isWeekday() {
        return isWeekday;
    }

    @Override
    public String toString() {
        return "Day{" +
                "isWeekday = " + isWeekday +
                '}';
    }
}
