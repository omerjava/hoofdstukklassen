package enumOpdracht;
/*
Als laatste gaan we een opsommingstype voor planeten maken

Maak een opsomming Planet met alle planeten van ons zonnestelsel.
Voeg hierbij de nodige eigenschappen voor de massa en de afstand tot de zon toe.
 */
public enum Planet {
    MERCURY("57900000", 0.33),
    VENUS("108200000", 4.87),
    EARTH("149600000", 5.97),
    MARS("227900000",0.642),
    JUPITER("778600000",1898),
    SATURN("1433500000",568),
    URANUS("2872500000", 86.8),
    NEPTUNE("4495100000", 102);

    private String distance;
    private double mass;

    Planet(String distance, double mass) {
        this.distance = distance;
        this.mass = mass;
    }

    public String getDistance() {
        return distance;
    }

    public double getMass() {
        return mass;
    }

    @Override
    public String toString() {
        return "Planet{" +
                "distance='" + distance + '\'' +
                ", mass=" + mass +
                '}';
    }
}
