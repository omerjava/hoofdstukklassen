package enumOpdracht;

/*
Vervolgens gaan we een opsommingstype voor munten maken

Maak een opsomming Coin voor alle mogelijke euromunten: ONE_CENT, TWO_CENT, …, TWO_EURO
Voeg een methode getValue() toe om de waarde (in centen) op te vragen
Maak een array van munten en vul deze met waarden. Bereken vervolgens de totale waarde van de array van munten
 */
public enum Coin {
    ONE_CENT(0.01),
    TWO_CENT(0.02),
    FIVE_CENT(0.05),
    TEN_CENT(0.1),
    TWENTY_CENT(0.2),
    FIFTY_CENT(0.5),
    ONE_EURO(1),
    TWO_EURO(2);

    private double value;

    Coin(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}
