import interfaceOpdracht.*;

public class InterfaceOpdracht {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[10];
        shapes[0] = new Circle(4);
        shapes[1] = new Rectangle(3,4,5,6);
        shapes[2] = new Square(5,6,4);
        shapes[3] = new Circle(3);
        shapes[4] = new Rectangle(2,4,3,5);
        shapes[5] = new Circle(9);
        shapes[6] = new Square(3,-5, 9);
        shapes[7] = new Rectangle(6,6,5,3);
        shapes[8] = new Square(5,8,8);
        shapes[9] = new Triangle(4,5,3,3,2);

        TextDrawingContext textDrawingContext = new TextDrawingContext();
        Drawing drawing = new Drawing();
        int count=0;
        for (Shape shape: shapes) {
            // System.out.println(shape.getClass().getName()+" Shape " + count+1 + " info:");

            System.out.println(" ");
            System.out.println("===========================");
            shape.draw(textDrawingContext);
            System.out.println(" ");
            drawing.draw(textDrawingContext);
            count++;
        }

    }

}
