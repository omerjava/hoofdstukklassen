package myShapes;

/*
Opdracht: De subklasse Square maken
Maak een nieuw broncodebestand voor de klasse Square. Geef het bestand de naam Square.java
Compileer het bestand
Maak een nieuw hoofdprogramma SquareAPp.java
Creëer in het hoofdprogramma een vierkant met de constructor zonder parameters
 */

public class Square extends Rectangle{

    private int side;

    public Square() {
        super();
    }

    public Square(int x, int y, int height, int width) {
        super(x, y, height, width);
    }

    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        super.setWidth(Math.abs(side));
        super.setHeight(Math.abs(side));
    }

    public void setSide(double side) {
        super.setWidth((int) Math.abs(side));
        super.setHeight((int) Math.abs(side));
    }

    @Override
    public void setWidth(int width) {
        setSide(width);
    }

    @Override
    public void setHeight(int height) {
        setSide(height);
    }

    public void setWidth( double width) {
        setSide(width);
    }

    public void setHeight(double height) {
        setSide(height);
    }
}
