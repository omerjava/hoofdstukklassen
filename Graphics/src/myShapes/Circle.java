package myShapes;

/*
  Opdracht 12: De klasse Circle
  In deze opdracht gaan we een nieuwe klasse Circle maken naar analogie van de klasse Rectangle.

  Maak een nieuwe klasse Circle en maak de nodige variabelen, methoden en constructors.
  Voor de berekening van de omtrek en de oppervlakte kan je een beroep doen op de klasse Math.
  Maak een hoofdprogramma CircleApp dat een cirkelobject maakt en druk de gegevens van het object op het scherm af.

   */
public class Circle {

    public static final int ANGLES = 0;
    private static int count;

    private int x;
    private int y;
    private int radius;

    // constructors

    // no arguments constructor
    public Circle() {
        count++;
    }

    public Circle(int radius) {
        this.radius = radius;
        count++;
    }

    // all arguments constructor
    public Circle(int radius, int x, int y) {
        this.radius = radius;
        this.x = x;
        this.y = y;
        count++;
    }

    // copy constructor
    public Circle(Circle circle) {
        this.radius = circle.radius;
        this.x = circle.x;
        this.y = circle.y;
        count++;
    }

    // setters
    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public void setPosition(int x, int y) {
        setX(x);
        setY(y);
    }

    // getters
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getRadius() {
        return radius;
    }

    // methods

    public double getArea(){
        return Math.PI*getRadius()*getRadius();
    }
    public double getPerimeter(){
        return 2*Math.PI*getRadius();
    }
    public void grow(int amount){
        setRadius(getRadius()+amount);
    }
    public static int getCount() {
        return count;
    }

}
