package myShapes;

public class Rectangle {
    public static final int ANGLES = 4;
    private static int count;

    private int x;
    private int y;
    private int height;
    private int width;


    // constructors
    public Rectangle() {
        count++;
    }

    public Rectangle(int x, int y, int height, int width) {
        setX(x);
        setY(y);
        setHeight(height);
        setWidth(width);
        count++;
    }

    // getters


    public int getWidth(){
        return this.width;
    }
    public int getHeight(){
        return this.height;
    }
    public int getX(){
        return this.x;
    }
    public int getY(){
        return this.y;
    }
    public double getArea(){
        return getHeight()*getWidth();
    }
    public double getPerimeter(){
        return this.height*2 + this.width*2;
    }


    // setters

    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y){
        this.y = y;
    }

    public void setWidth( int width){
        this.width = width;
    }

    public void setHeight(int height){
        this.height = height;
    }

    // methods
    public void grow(int amount){
        setHeight(getHeight()+amount);
        setWidth(getWidth()+amount);
    }

    public void setPosition(int x, int y) {
        setX(x);
        setY(y);
    }

    public static String writeHello(){
        return "\nHallo iedeeren!";
    }

    public static int getCount() {
        return count;
    }

}
