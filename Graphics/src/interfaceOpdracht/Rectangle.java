package interfaceOpdracht;

public class Rectangle extends Shape implements Scalable {
    private static int count;
    {
        count++;
    }
    public static int VERTICES = 4;

    private int width;
    private int height;

    public Rectangle() {
        super();
    }

    public Rectangle(int width, int height) {
        setWidth(width);
        setHeight(height);
    }

    public Rectangle(int x, int y, int width, int height) {
        super(x, y);
        setWidth(width);
        setHeight(height);
    }

    public Rectangle(Rectangle rectangle) {
        this(rectangle.getX(), rectangle.getY(), rectangle.getWidth(), rectangle.getWidth());
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width<0 ? Math.abs(width) : width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height<0 ? Math.abs(height) : height;
    }

    @Override
    public double getArea() {
        return getHeight()*getWidth();
    }

    @Override
    public double getPerimeter() {
        return 2*(getHeight()+getWidth());
    }

    public void grow(int amount){
        setWidth(getWidth()+amount);
        setHeight(getHeight()+amount);
    };

    public static int getCount(){
        return count;
    };

    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rectangle rectangle = (Rectangle) o;

        if (width != rectangle.width) return false;
        return height == rectangle.height;
    }

    @Override
    public int hashCode() {
        int result = width;
        result = 31 * result + height;
        return result;
    }

    @Override
    public void scale(int factor) {
        System.out.println("Rectangle concrete class is used with scale function!");
        System.out.println(factor);
    }

    @Override
    public void scaleDouble(int factor) {
        System.out.println("Rectangle concrete class is used with scaleDouble function!");
        System.out.println(factor);
    }

    @Override
    public void scaleHalf(int factor) {
        System.out.println("Rectangle concrete class is used with scaleHalf function!");
        System.out.println(factor);
    }

    @Override
    public void draw(DrawingContext drawingContext) {
        System.out.println("Rectangle concrete class is used with draw function!");
        System.out.println(drawingContext);
    }
}
