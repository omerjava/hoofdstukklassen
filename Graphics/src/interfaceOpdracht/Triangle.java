package interfaceOpdracht;

public class Triangle extends Shape {
    private static int count;
    { count++; }

    public int VERTICES = 3;

    private int width;
    private int height;
    private int perpendicular;

    public Triangle() {
        super();
    }

    public Triangle(int width, int height, int perpendicular) {
        setWidth(width);
        setHeight(height);
        setPerpendicular(perpendicular);
    }

    public Triangle(int x, int y, int width, int height, int perpendicular) {
        super(x, y);
        setWidth(width);
        setHeight(height);
        setPerpendicular(perpendicular);
    }

    public Triangle(Triangle triangle) {
        this(triangle.getX(), triangle.getY(), triangle.getWidth(), triangle.getHeight(), triangle.getPerpendicular());
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width<0 ? Math.abs(width) : width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height<0 ? Math.abs(height) : height;
    }

    public int getPerpendicular() {
        return perpendicular;
    }

    public void setPerpendicular(int perpendicular) {
        this.perpendicular = perpendicular<0 ? Math.abs(perpendicular) : perpendicular;;;
    }

    @Override
    public double getArea() {
        return (getHeight()*getPerpendicular())/2;
    }

    @Override
    public double getPerimeter() {
        return getWidth()+getHeight()+getPerpendicular();
    }

    public void grow(int amount){
        setWidth(getWidth()+amount);
        setHeight(getHeight()+amount);
    }

    public static int getCount(){
        return count;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "VERTICES=" + VERTICES +
                ", width=" + width +
                ", height=" + height +
                ", perpendicular=" + perpendicular +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Triangle triangle = (Triangle) o;

        if (VERTICES != triangle.VERTICES) return false;
        if (width != triangle.width) return false;
        if (height != triangle.height) return false;
        return perpendicular == triangle.perpendicular;
    }

    @Override
    public int hashCode() {
        int result = VERTICES;
        result = 31 * result + width;
        result = 31 * result + height;
        result = 31 * result + perpendicular;
        return result;
    }

    @Override
    public void draw(DrawingContext drawingContext) {
        System.out.println("Triangle concrete class is used with draw function!");
        System.out.println(drawingContext);
    }

    @Override
    public void scale(int factor) {
        System.out.println("Triangle concrete class is used with scale function!");
        System.out.println(factor);
    }

    @Override
    public void scaleDouble(int factor) {
        System.out.println("Triangle concrete class is used with scaleDouble function!");
        System.out.println(factor);
    }

    @Override
    public void scaleHalf(int factor) {
        System.out.println("Triangle concrete class is used with scaleHalf function!");
        System.out.println(factor);
    }
}
