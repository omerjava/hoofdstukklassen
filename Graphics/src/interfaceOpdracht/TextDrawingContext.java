package interfaceOpdracht;

public class TextDrawingContext implements DrawingContext {


    @Override
    public void draw(Rectangle rectangle) {
        System.out.println("Area of Rectangle: "+rectangle.getArea());
        System.out.println("Perimeter of Rectangle: "+rectangle.getPerimeter());
    }

    @Override
    public void draw(Circle circle) {
        System.out.println("Area of Rectangle: "+circle.getArea());
        System.out.println("Perimeter of Rectangle: "+circle.getPerimeter());
    }

    @Override
    public void draw(Triangle triangle) {
        System.out.println("Area of Triangle: "+triangle.getArea());
        System.out.println("Perimeter of Triangle: "+triangle.getPerimeter());
    }
}
