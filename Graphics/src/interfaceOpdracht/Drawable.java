package interfaceOpdracht;

public interface Drawable extends Scalable {

    void draw(DrawingContext drawingContext);

}
