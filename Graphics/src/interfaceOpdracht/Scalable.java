package interfaceOpdracht;

public interface Scalable {
        int DOUBLE = 200;
        int HALF = 50;
        int QUARTER = 25;

        void scale(int factor);
        void scaleDouble(int factor);
        void scaleHalf(int factor);
}
