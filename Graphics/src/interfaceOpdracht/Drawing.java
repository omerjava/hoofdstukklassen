package interfaceOpdracht;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Drawing implements Drawable {

    public Drawing() {}
    private int size;
    Shape[] shapeObjects = new Shape[0];


    public void add(Drawable drawable){
        List<Drawable> drawableList = new ArrayList<>(Arrays.asList(shapeObjects));
        if(drawableList.size()<=100) {
            drawableList.add(drawable);
        } else {
            System.out.println("There is no more place in Array!");
        }
        shapeObjects = drawableList.toArray(shapeObjects);
        if(shapeObjects[shapeObjects.length-1].equals(drawable)) {
            System.out.println("Test is ok. Given shape is added to Tekening..");
        }
        else {
            System.out.println("Oh..no! Something wrong!");
        }
    }

    public void remove(Drawable drawable){
        List<Drawable> drawableList = new ArrayList<>(Arrays.asList(shapeObjects));
        drawableList.remove(drawable);
        shapeObjects = drawableList.toArray(shapeObjects);
        if(!shapeObjects[shapeObjects.length-1].equals(drawable)) {
            System.out.println("Test is ok. Given shape is removed from Tekening..");
        }
        else {
            System.out.println("Oh..no! Something wrong!");
        }
    }

    public void clear(){
        List<Shape> shapeList = new ArrayList<>(Arrays.asList(shapeObjects));
        shapeList.clear();
        shapeObjects = shapeList.toArray(shapeObjects);
    }

    public int getSize(){
        size = shapeObjects.length;
        return size;
    }


    @Override
    public void draw(DrawingContext drawingContext) {
        System.out.println("Drawing class is used with draw function!");
        System.out.println(drawingContext);
    }

    @Override
    public void scale(int factor) {
        System.out.println("Drawing class is used with scale function!");
        System.out.println(factor);
    }

    @Override
    public void scaleDouble(int factor) {
        System.out.println("Drawing class is used with scaleDouble function!");
        System.out.println(factor);
    }

    @Override
    public void scaleHalf(int factor) {
        System.out.println("Drawing class is used with scaleHalf function!");
        System.out.println(factor);
    }

    @Override
    public String toString() {
        return "Drawing{" +
                "size=" + size +
                ", shapeObjects=" + Arrays.toString(shapeObjects) +
                '}';
    }
}
