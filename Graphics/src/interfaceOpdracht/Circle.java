package interfaceOpdracht;

public class Circle extends Shape {
    private static int count;
    {
        count++;
    }

    public static int VERTICES = 0;

    private int radius;

    public Circle() {
        super();
    }

    public Circle(int radius) {
        setRadius(radius);
    }

    public Circle(int x, int y, int radius) {
        super(x, y);
        setRadius(radius);
    }

    public Circle(Circle circle) {
        this(circle.getX(), circle.getY(), circle.getRadius());
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius<0 ? Math.abs(radius) : radius;
    }

    @Override
    public double getArea() {
        return Math.PI*getRadius()*getRadius();
    }

    @Override
    public double getPerimeter() {
        return 2*Math.PI*getRadius();
    }

    public void grow(int amount){
        setRadius(getRadius()+amount);
    };

    public static int getCount(){
        return count;
    };

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Circle circle = (Circle) o;

        return radius == circle.radius;
    }

    @Override
    public int hashCode() {
        return radius;
    }

    @Override
    public void draw(DrawingContext drawingContext) {
        System.out.println("Circle concrete class is used with draw function!");
        System.out.println(drawingContext);
    }

    @Override
    public void scale(int factor) {
        System.out.println("Circle concrete class is used with scale function!");
        System.out.println(factor);
    }

    @Override
    public void scaleDouble(int factor) {
        System.out.println("Circle concrete class is used with scaleDouble function!");
        System.out.println(factor);
    }

    @Override
    public void scaleHalf(int factor) {
        System.out.println("Circle concrete class is used with scaleHalf function!");
        System.out.println(factor);
    }
}
