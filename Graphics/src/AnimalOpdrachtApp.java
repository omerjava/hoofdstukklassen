import opdrachtAnimalAbstractClass.*;

public class AnimalOpdrachtApp {
    public static void main(String[] args) {
        Cat cat1 = new Cat("pamuk");
        System.out.println(cat1.getName());
        cat1.makeNoise();
        cat1.move();

        Animal cat2 = new Cat("kitty");
        System.out.println(cat2.getName());
        cat2.makeNoise();
        cat2.move();

        Animal bird = new Bird("black eagle");
        System.out.println(bird.getName());
        bird.makeNoise();
        bird.move();

        Fish fish = new Fish("cool dolphin");
        System.out.println(fish.getName());
        fish.makeNoise();
        fish.move();

        Snake snake = new Snake("angry snake");
        System.out.println(snake.getName());
        snake.makeNoise();
        snake.move();

        String str = new String("hello");
        String str2 = new String("hello");

        System.out.println(str == str2);



    }
}
