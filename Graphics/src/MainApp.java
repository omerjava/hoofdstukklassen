import myShapes.Circle;
import myShapes.Rectangle;

public class MainApp {
    /*
    Opdracht 3: variabelen toevoegen
In deze opdracht gaan we de instance-variabelen aan de klasse Rectangle toevoegen.
Tevens gaan we experimenteren met het toegangsniveau van de variabelen.

Voeg de variabelen height, width, x en y toe aan de klasse definitie.
Ken in het hoofdprogramma expliciet een waarde toe aan deze variabelen.
Druk de hoogte, breedte en de positie van de rechthoek af op het scherm.
Maak een tweede rechthoek en geef deze andere afmetingen en een andere positie.
Druk de gegevens van deze tweede rechthoek af.
verander het toegangsniveau van de variabelen achtereenvolgens in protected, -(package) en private. Compileer de klasse en voer het programma opnieuw uit. Welk toegangsniveau werkt en waarom?
Zet het toegangsniveau van de variabelen terug op public.
     */
    public static void main(String[] args) {
        Rectangle rect = new Rectangle();
        rect.setPosition(20, 30);
        rect.setX(50);
        rect.setY(21);
        rect.setHeight(10);
        rect.setWidth(32);

        System.out.println(rect.getWidth());
        System.out.println(rect.getHeight());
        System.out.println("Rectangle perimeter: " + rect.getPerimeter());

        Rectangle rectangle = new Rectangle();

        rectangle.setWidth(22);
        rectangle.setHeight(24);
        rectangle.setX(5);
        rectangle.setY(9);

        rectangle.setPosition(80, 90);
        System.out.println("        ");
        System.out.println("X: " + rectangle.getX());
        System.out.println("Y: " + rectangle.getY());
        System.out.println("Width: " + rectangle.getWidth());
        System.out.println("Height: " + rectangle.getHeight());
        System.out.println("Perimeter: " + rectangle.getPerimeter());
        System.out.println("Area: " + rectangle.getArea());


        Rectangle rect3 = new Rectangle(40, 20, 15, 25);
        System.out.println("\nRect3:        ");
        System.out.println("X: " + rect3.getX());
        System.out.println("Y: " + rect3.getY());
        System.out.println("Width: " + rect3.getWidth());
        System.out.println("Height: " + rect3.getHeight());
        System.out.println("Perimeter: " + rect3.getPerimeter());
        System.out.println("Area: " + rect3.getArea());

        System.out.println(Rectangle.writeHello());

        Circle circle = new Circle(4, 9,21);
        System.out.println("\nCircle:        ");
        System.out.println("X: " + circle.getX());
        System.out.println("Y: " + circle.getY());
        System.out.println("Circle Radius: " + circle.getRadius());
        System.out.println("Circle Perimeter: " + circle.getPerimeter());
        System.out.println("Circle Area: " + circle.getArea());

        Circle circle2 = new Circle(circle);
        circle2.setPosition(40,45);
        System.out.println("Circle2 X: " + circle2.getX());
        System.out.println("Circle2 Y: " + circle2.getY());
        System.out.println("Circle2 Radius: " + circle2.getRadius());
        System.out.println("Circle2 Perimeter: " + circle2.getPerimeter());
        System.out.println("Circle2 Area: " + circle2.getArea());

        Circle circle3 = new Circle();
        circle3.setPosition(3,4);
        circle3.setRadius(6);
        System.out.println("Circle3 X: " + circle3.getX());
        System.out.println("Circle3 Y: " + circle3.getY());
        System.out.println("Circle3 Radius: " + circle3.getRadius());
        System.out.println("Circle3 Perimeter: " + circle3.getPerimeter());
        System.out.println("Circle3 Area: " + circle3.getArea());
        System.out.println(Circle.getCount());
        circle3.grow(10);
        System.out.println("Circle3 Radius: " + circle3.getRadius());
        System.out.println("Circle3 Perimeter: " + circle3.getPerimeter());
        System.out.println("Circle3 Area: " + circle3.getArea());    }
}
