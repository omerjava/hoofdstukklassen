import enumOpdracht.Day;

public class DaysApp {
    public static void main(String[] args) {

        for (Day element : Day.values()) {
            System.out.println(element.name()+ " " + element.ordinal());
            System.out.println(element);
        }
    }
}
