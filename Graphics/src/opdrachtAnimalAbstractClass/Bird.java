package opdrachtAnimalAbstractClass;

public class Bird extends Animal {

    public Bird() {
        super();
    }

    public Bird(String name) {
        super(name);
    }

    @Override
    public void move() {
        System.out.println(getName()+ " is flying.");
    }

    @Override
    public void makeNoise() {
        System.out.println(getName()+ " tweeting. Hey, I am a bird!");
    }
}
