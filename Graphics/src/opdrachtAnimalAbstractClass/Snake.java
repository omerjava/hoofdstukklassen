package opdrachtAnimalAbstractClass;

public class Snake extends Animal {

    public Snake() {
        super();
    }

    public Snake(String name) {
        super(name);
    }

    @Override
    public void move() {
        System.out.println(getName()+ " is crawling");
    }

    @Override
    public void makeNoise() {
        System.out.println(getName()+ " tissssss. Watch out, I am snake!");
    }
}
