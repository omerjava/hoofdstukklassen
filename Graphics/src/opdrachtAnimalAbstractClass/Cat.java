package opdrachtAnimalAbstractClass;

public class Cat extends Animal {

    public Cat() {
        super();
    }

    public Cat(String name) {
        super(name);
    }

    @Override
    public void move() {
        System.out.println(getName()+" is walking.");
    }

    @Override
    public void makeNoise() {
        System.out.println(getName()+ " miauwww. Watch out, I am the cat!");
    }
}
