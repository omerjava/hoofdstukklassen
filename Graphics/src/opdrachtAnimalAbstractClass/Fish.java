package opdrachtAnimalAbstractClass;

public class Fish extends Animal {

    public Fish() {
        super();
    }

    public Fish(String name) {
        super(name);
    }

    @Override
    public void move() {
        System.out.println(getName()+ " is swimming.");
    }

    @Override
    public void makeNoise() {
        System.out.println(getName()+ " shsissdashsduasuda. The screams in the ocean!");
    }
}
