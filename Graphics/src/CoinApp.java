import enumOpdracht.Coin;

public class CoinApp {
    public static void main(String[] args) {
        Coin[] coins = new Coin[5];
        coins[0] = Coin.FIFTY_CENT;
        coins[1] =Coin.ONE_CENT;
        coins[2] = Coin.TWO_EURO;
        coins[3] = Coin.ONE_EURO;
        coins[4] = Coin.TEN_CENT;

        double total=0;
        for (Coin coin : coins) {
            total+=coin.getValue();
        }
        System.out.println("Total value of coins in array: " + total + "€");
    }
}
