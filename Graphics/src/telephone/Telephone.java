package telephone;

public class Telephone {
    private String brand;
    private String color;
    private double price;
    private String os;

    public Telephone(String brandPhone, String colorPhone, double pricePhone, String osPhone) {
        setBrand(brandPhone);
        setColor(colorPhone);
        setPrice(pricePhone);
        setOs(osPhone);
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }
}
