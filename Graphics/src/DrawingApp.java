import interfaceOpdracht.Drawing;
import interfaceOpdracht.Rectangle;
import interfaceOpdracht.Shape;
import interfaceOpdracht.Square;

public class DrawingApp {
    public static void main(String[] args) {
        Drawing drawing1 = new Drawing();
        Shape square1 = new Square(3,4,7);
        drawing1.add(square1);
        System.out.println(drawing1.getSize());

        Shape rect1 = new Rectangle(4,5,3,2);
        drawing1.add(rect1);
        System.out.println(drawing1.getSize());

        Shape rect2 = new Rectangle(4,5,8,2);
        drawing1.add(rect2);
        System.out.println(drawing1.getSize());


    }
}
