package wrapperAndDateClasses;

import java.time.Instant;

public class InstantOpdracht {
/*
Opdracht: Instant
Maak een programma dat bij de huidige tijd 42 seconden, 9 milliseconden en 4 nanoseconden bijtelt
 */
    public static void main(String[] args) {
        Instant huidigeTijdstip = Instant.now();
        System.out.println("Current time:         " + huidigeTijdstip);

        Instant revisedTijdstip = huidigeTijdstip.plusSeconds(42);
        System.out.println("42 seconds are added: " + revisedTijdstip);

        revisedTijdstip = revisedTijdstip.plusMillis(9);
        System.out.println("9 miliseconds are added: " + revisedTijdstip);

        revisedTijdstip = revisedTijdstip.plusNanos(4);
        System.out.println("4 nanoseconds are added: " + revisedTijdstip);
    }
}
