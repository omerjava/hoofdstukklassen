package wrapperAndDateClasses;

import java.time.DayOfWeek;
import java.time.Month;
import java.util.Arrays;
import java.util.Scanner;

/*
Opdracht: Weekdagen en maanden
Open de API-documentatie van de opsommingstypes Month en DayOfWeek
Maak een programma dat de gebruiker vraagt een weekdag in te geven (1-7) en
een aantal dagen om erbij op te tellen. Druk af welke dag van de week dit is.
 */
public class MonthAndWeekOpdracht {

    public static void main(String[] args) {
        System.out.println(Month.APRIL.toString());
        System.out.println(Month.AUGUST.getValue());
        System.out.println(Arrays.toString(Month.values()));
        System.out.println(" ");
        System.out.println(DayOfWeek.MONDAY.toString());
        System.out.println(Arrays.toString(DayOfWeek.values()));

        System.out.println("\nEnter a value from 1-7: ");
        Scanner userInput = new Scanner(System.in);
        int input = userInput.nextInt();

        System.out.println(DayOfWeek.of(input));


    }
}
