package wrapperAndDateClasses;

/*
Opdracht: Formattering
Overloop de API-documentatie van de klasse DateTimeFormatter en besteed aandacht aan de formatteringskarakters
Maak een programma dat de gebruiker vraagt een datum in te geven in het formaat DD/MM/YYYY
Zet de bekomen tekst om in een object van de klasse LocalDate en druk de inhoud af in het formaat YYYY-MM-DD
 */

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class FormatDateOpdracht {
    public static void main(String[] args) {
        LocalDate today = LocalDate.now();
        System.out.println(today);

        DateTimeFormatter majorityFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        System.out.println(today.format(majorityFormat));

        DateTimeFormatter usaFormat = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        System.out.println(today.format(usaFormat));

        LocalDate date = LocalDate.parse("03/26/2020", usaFormat);
        System.out.println(date);

        // opdracht
        System.out.println("Enter a date pls in this format -> DD/MM/YYYY: ");
        Scanner userInput = new Scanner(System.in);
        String userDate = userInput.nextLine();

        DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate userInputDate = LocalDate.parse(userDate.trim(), inputFormat);
        System.out.println("Opdracht formatted date: " +
                userInputDate.format(DateTimeFormatter.ofPattern("yyyy/MM/dd")));


        /*

        System.out.println("Enter a date pls in this format -> DD/MM/YYYY: ");
        Scanner keyboard = new Scanner(System.in);
        String answer = keyboard.next();
        LocalDate dateLast = LocalDate.parse(answer,DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        System.out.println( dateLast.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

         */
    }
}
