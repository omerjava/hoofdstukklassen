package wrapperAndDateClasses;

import java.time.*;
import java.time.temporal.ChronoUnit;

public class Tijdsuur {
    /*
    Opdracht: Tijdsduur
    Overloop in de API-documentatie de klassen Duration, ChronoUnit en Period
    Maak een programma dat de periode berekent sinds je geboorte. Druk van deze periode het volgende af:
        – het aantal dagen
        – het aantal maanden
        – het aantal jaren
    Druk ook het totaal aantal dagen, weken en maanden af sinds je geboorte
     */
    public static void main(String[] args) {
        Instant now = Instant.now();
        Instant later = now.plusSeconds(3600);

        // Duration
        Duration duration = Duration.between(now, later);

        // ChronoUnit
        long minutes = ChronoUnit.MINUTES.between(now, later);

        // Period
        LocalDate dateOfBirth = LocalDate.of(1982, Month.SEPTEMBER, 23);
        LocalDate today = LocalDate.now();


        Period period = Period.between(dateOfBirth, today);

        System.out.println("Period since birthday: "+period);
        System.out.println("Years since birthday: "+period.getYears());
        System.out.println("Months since birthday: "+period.getMonths());
        System.out.println("Days since birthday: "+period.getDays());
    }
}
