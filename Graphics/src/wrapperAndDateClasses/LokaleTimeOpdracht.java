package wrapperAndDateClasses;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;

public class LokaleTimeOpdracht {
    /*
    Opdracht: Lokale datums en tijden
Overloop de API-documentatie van de klassen LocalDate, LocalTime en LocalDateTime
Maak een programma dat van je geboortedatum het volgende afdrukt
– de hoeveelste dag van het jaar het was (bv. dag 327 van 365)
– de dag van de maand (bv. 23)
– de dag van de week (bv. WEDNESDAY)
– of dit al dan niet een schrikkeljaar was (bv. false)
     */
    public static void main(String[] args) {
        LocalDateTime nowDateTime = LocalDateTime.now();
        System.out.println(nowDateTime);

        LocalTime lunchTime = LocalTime.of(12, 30, 0);
        System.out.println(lunchTime);

        LocalDate myDateOfBirth = LocalDate.of(1982, Month.SEPTEMBER, 5);
        System.out.println(myDateOfBirth);
        System.out.println(myDateOfBirth.getDayOfYear());
        System.out.println(myDateOfBirth.getDayOfMonth());
        System.out.println(myDateOfBirth.getDayOfWeek());
        System.out.println(myDateOfBirth.isLeapYear());

    }
}
