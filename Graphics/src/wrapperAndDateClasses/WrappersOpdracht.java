package wrapperAndDateClasses;

import java.util.Scanner;

/*
Opdracht: Wrappers
In deze opdracht verkennen we de wrapper-klasse Integer

Open de API-documentatie van de klasse Integer en verken de verschillende eigenschappen en
methoden van deze klasse
Maak een programma dat de gebruiker vraagt om een waarde in te geven. Doe dit met de volgende regels code:
Scanner keyboard = new Scanner(System.in);
String input = keyboard.next();
Zet de String-waarde om in een Integer-object
Tel bij dit object een waarde op en druk het resultaat af
Druk het aantal bits voor een Integer af
Druk het aantal bytes voor een integer af
 */

public class WrappersOpdracht {

    public static void main(String[] args) {
        System.out.println("Enter a value: ");
        Scanner keyboard = new Scanner(System.in);
        String input = keyboard.next();

        Integer integer = Integer.valueOf(input);

        System.out.println("Integer plus 5: "+(integer+5)); // numbers are summed and added to a string
        System.out.println("int plus 5: "+(integer.intValue()+5)); // numbers are summed and added to a string
        System.out.println("Integer plus 5: "+integer+5);  // string plus numbers

    }
}
