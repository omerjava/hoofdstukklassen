package wrapperAndDateClasses;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class TijdzonesOpdracht {
    /*
    Opdracht: Tijden met tijdszones
Overloop in de API-documentatie de klasse ZonedDateTime
Maak een programma dat de huidige tijd afdrukt in de volgende steden:
– je huidige plaats
– Londen (Europe/London)
– Sydney (Australia/Sydney)
– Adelaide (Australia/Adelaide)
– UTC-4
     */
    public static void main(String[] args) {
        ZoneId londonZone = ZoneId.of("Europe/London");
        System.out.println(londonZone);

        ZonedDateTime nowLondon = ZonedDateTime.now(londonZone);
        System.out.println(nowLondon);

        ZoneId systemZoneId = ZoneId.systemDefault();
        System.out.println(systemZoneId);

        ZoneOffset timeZone = ZoneOffset.ofHours(-12);
        System.out.println(timeZone);

        String[] timezones = {"Europe/Brussels", "Europe/London", "Australia/Adelaide", "Australia/Sydney" };

        for (String zone: timezones) {
            ZoneId zoneId = ZoneId.of(zone);
            System.out.println(" ");
            System.out.println(ZonedDateTime.now(zoneId));
        }


        ZoneOffset utc4 = ZoneOffset.ofHours(-4);
        System.out.println(ZoneId.ofOffset("UTC", utc4));

        System.out.println(ZonedDateTime.now(londonZone).minusHours(4));

    }
}
